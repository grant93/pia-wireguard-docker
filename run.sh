#!/bin/bash
# Handle shutdown behavior
finish () {
    echo "$(date): Shutting down Wireguard"
    wg-quick down pia 
    exit 0
} 
  
connect () {
    # allow out for initial setup
    ufw default allow outgoing
    # setup PIA
    ./run_setup.sh

    # only allow outbound over vpn
    ufw default deny outgoing
    . /tmp/env
    echo "Allowing traffic to ${WG_SERVER_IP} ${WG_SERVER_PORT}"
    ufw allow out to $WG_SERVER_IP port $WG_SERVER_PORT 
    echo "Allowing DNS traffic to ${WG_DNS_IP} port 53"
    ufw allow out to $WG_DNS_IP port 53

}
echo "starting"
connect

# sleep util signal
trap finish SIGTERM SIGINT SIGQUIT
  
while :
do
    if ! nc -zw1 google.com 443; then
        connect
    fi

    sleep 600
done
