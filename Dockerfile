FROM alpine:latest
RUN apk add --no-cache curl jq netcat-openbsd patch ufw wireguard-tools 
COPY . /pia-wg
ENV VPN_PROTOCOL="wireguard" \
    PREFERRED_REGION="us-newjersey" \
    PIA_USER="" \
    PIA_PASS="" \
    MAX_LATENCY=2 \
    PIA_DNS=true \
    AUTOCONNECT=false \
    DISABLE_IPV6=yes \
    PIA_PF=false

RUN patch /pia-wg/manual-connections/connect_to_wireguard_with_token.sh /pia-wg/pia.diff
RUN patch /usr/bin/wg-quick /pia-wg/wg-quick.diff

WORKDIR /pia-wg/manual-connections
COPY run.sh .

ENTRYPOINT ["./run.sh"]
